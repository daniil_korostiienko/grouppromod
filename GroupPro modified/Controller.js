var mediator = new Mediator(),
    group = new Group('DP-070'),
    groupView = new GroupView(),
    groupMemberView = new GroupMemberView(),
    personView = new PersonView(),
    previewView = new PreviewView();

window.addEventListener('load', init(), false);

function init () {
  mediator.subscribe('showPersonInGroupList', groupMemberView.addPersonToList);
  mediator.subscribe('refreshPersonInGroupList', groupMemberView.refreshPersonInList);

  mediator.subscribe('showPerson', personView.showPerson);
  mediator.subscribe('savePerson', personView.savePerson);
  mediator.subscribe('requestActivePerson', personView.getActivePerson);
  mediator.subscribe('hidePerson', personView.hidePerson);
  mediator.subscribe('deletePerson', personView.deletePerson);

  mediator.subscribe('showPreview', previewView.showPreview);
  mediator.subscribe('hidePreview', previewView.hidePreview);
  mediator.subscribe('activePerson', previewView.updateActivePerson);

  mediator.subscribe('personCreated', groupView.refreshList);
  mediator.subscribe('personDeleted', groupView.refreshList);

  mediator.subscribe('createPerson', function() {
    group.addStudent(null);
  }, false);

  groupView.init();
}