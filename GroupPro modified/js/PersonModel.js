function Person (_name) {
  var attributes = {
      "name" : _name,
      "surname" : '',
      "middleName" : '',
      "passport" : '',
      "taxNumber" : '',
      "gender" : '',
      "age": ''
  };
  
  this.set = function (key, value) {
    attributes[key] = value;
  };
  
  this.get = function (key) {
    return attributes[key];
  };

  this.getAttributes = function () {
    return attributes;
  };

  this.setAll = function (person) {
    console.log(person);
    _.forEach(person, function(attributeValue, attributeKey) {
      if (attributeValue !== '') {
        attributes[attributeKey] = attributeValue;
      }
    });
  };
  
  this.getAll = function () {
    var attributeStorage = [];

    _.forEach(attributes, function(attributeValue, attributeKey) {
        attributes[attributeKey] = attributeValue;
      attributeStorage.push(attributeValue);
    });
    
    return attributeStorage;
  };

  return this;
}
