function Group (groupName) {
  var name = groupName,
      students = [];

  this.addStudent = function (person) {
    students.push(new Person());
    if (!person) {
      person = {"name":"New", "surname": String(group.getLength())};
    }
    students[students.length-1].setAll(person);
    mediator.publish('personCreated');
  };

  this.getName = function () {
    return name;
  };
  
  this.setName = function (_name) {
    name = _name;
  };
  
  this.setStudent = function (studentId, attributeValues) {  //set student's attribute
    students[studentId].setAll(attributeValues);
  };
  
  this.getStudent = function (studentId) {  //get student's attribute
    return students[studentId].getAll();
  };
  
  this.getStudents = function () {  //show name and students
    console.log(name);
    _.forEach(students, function(student, studentId){
      console.log(student.getAll());
    });
  };
  
  this.deleteStudent = function (studentId) {
    students.splice(studentId, 1);
  };
  
  this.getLength = function () {
    return students.length;
  };

  this.getData = function () {
    var ajax = new getAjax(),
        someData;
    ajax.open('GET', '/getData', true);
    ajax.setRequestHeader('Content-Type', 'text/plain');
    ajax.send();
    ajax.onreadystatechange = function() {
      if (ajax.readyState == 4) {
        if(ajax.status == 200) {
          someData = JSON.parse(ajax.responseText);
        } else {
          someData = ajax.responseText;
        }
      }
      _.forEach(someData, function(personAttr, personKey){
        group.addStudent(personAttr);
      });
    };
  };

  function getAjax () {
    var ajax;
    try {
      ajax = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (E) {
        ajax = false;
      }
    }
    if (!ajax && typeof XMLHttpRequest!='undefined') {
      ajax = new XMLHttpRequest();
    }
    return ajax;
  }

  return this;
}

