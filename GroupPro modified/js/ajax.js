var http = require('http'),
    fs = require('fs');

function onRequest (request, response) {
  var path = request.url;
  console.log(path);
  switch(path) {
    case '/':
      fs.readFile("../JS Mod.4.html", "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/plain"});
          response.write('404 Index.html Not Found' + "\n");
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/html"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;
    case '/getData':
      fs.readFile("text.json", "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/json"});
          response.write('404 text.json Not Found');
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/plain"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;
    case '/JS3CSS.css':
      fs.readFile('../JS3CSS.css', "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/text"});
          response.write('404 text.txt Not Found');
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/css"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;
    default:
      fs.readFile('../' + path, "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/plain"});
          response.write(path + ' Not Found');
          console.log(path + ' Not Found');
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/plain"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;
  }
}

http.createServer(onRequest).listen(8888);
