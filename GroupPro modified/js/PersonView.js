function PersonView () {
  var $shortView, $tabs, $inputs, $buttons, $serviceButtons,
      activePerson, i, j;

  this.init = function () {
    var templateSwitcherTab  = _.template('<div class="switcherTab">' +
          '<button class="button">Names</button>' +
          '<button class="button">Passport</button>' +
          '<button class="button">Gender/Age</button>' +
          '<button class="serviceButtons saveButton">Save</button>' +
          '<button class="serviceButtons previewButton">Preview</button>' +
          '<button class="serviceButtons hideAllButton">Hide</button>' +
          '<button class="serviceButtons deleteButton">Delete</button>' +
        '</div>'),

        templateTabs  = _.template('<div class="tab">' +
          '<p>Name</p>' +
          '<input class="input" name="name"></input>' +
          '<p>Surname</p>' +
          '<input class="input" name="surname"></input>' +
          '<p>Middle name</p>' +
          '<input class="input" name="middleName"></input>' +
        '</div>' +
        '<div class="tab hidden">' +
          '<p>Passport</p>' +
          '<input class="input" name="passport"></input>' +
          '<p>TaxNumber</p>' +
          '<input class="input" name="taxNumber"></input>' +
        '</div>' +
        '<div class="tab hidden">' +
          '<p>Gender</p>' +
          '<input class="input" name="gender"></input>' +
          '<p>Age</p>' +
          '<input class="input" name="age"></input>' +
        '</div>');

    $shortView = $('.shortView');
    $shortView.html(templateSwitcherTab());
    $shortView.append(templateTabs());
    $tabs = $shortView.find('.tab');
    $inputs = $shortView.find('.input');
    $buttons = $shortView.find('.button');
    $serviceButtons = $shortView.find('.serviceButtons');

    _.forEach($buttons, function (button, index) {
      $buttons.eq(index).click(function() {
        return function() {
          manageAll(index);
        }
      }(index));
    });

    $serviceButtons.eq(0).click(function () {
      mediator.publish('savePerson');
    });
    $serviceButtons.eq(1).click(function () {
      mediator.publish('showPreview', activePerson);
    });
    $serviceButtons.eq(2).click(function () {
      mediator.publish('hidePreview');
      mediator.publish('hidePerson');
    });
    $serviceButtons.eq(3).click(function () {
      mediator.publish('deletePerson');
    });
  };


  this.showPerson = function (personId) {
    var person = group.getStudent(personId);

    personView.init();
    activePerson = personId;
    console.log(person);

    mediator.publish('hidePreview');
    $shortView.removeClass('hidden');

    i = 0;
    _.forEach(person, function (attribute, index) {
      if (attribute !== '') {
        $inputs.eq(i).val(attribute);
      }
      else {
        $inputs.eq(i).val('');
      }
      i++;
    });
  };

  this.savePerson = function () {
    var attributes = {
      "name" : '',
      "surname" : '',
      "middleName" : '',
      "passport" : '',
      "taxNumber" : '',
      "gender" : '',
      "age": ''
      },
      i = 0;

    _.forEach(attributes, function (attrValue, key) {
      attributes[key] = $inputs.eq(i).val();
      i++;
    });

    group.setStudent(activePerson, attributes);
    mediator.publish('refreshPersonInGroupList', activePerson);
  };

  this.getActivePerson = function () {
    mediator.publish('activePerson', activePerson);
  };

  this.hidePerson = function () {
    $shortView.addClass('hidden');
  };

  this.deletePerson = function () {
    group.deleteStudent(activePerson);
    mediator.publish('hidePerson');
    mediator.publish('hidePreview');
    mediator.publish('personDeleted');
  };

  function manageAll (i) { // Tabs switcher, hides all, then shows tab[i]
    for (j = 0; j < $tabs.length; j++) {
      $tabs.eq(j).addClass('hidden');
    }

    $tabs.eq(i).removeClass('hidden');
  }

  return this;
}
