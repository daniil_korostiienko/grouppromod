function PreviewView () {
  var $previewTab = $('.previewTab'),
      activePerson, i, j;

  this.showPreview = function (personId) {
    var attributes = ['Name', 'Surname', 'MiddleName', 'Passport', 'TaxNumber', 'Gender', 'Age'],
        templateList  = _.template('<ul class="preview"></ul><button class="serviceButtons hidePreviewButton">Close</button>'),
        templateListElement  = _.template('<li><%= attribute %>: <%= value %></li>'),
        $preview,
        $hidePreviewButton;

    if (personId) {
      activePerson = personId;
    } else {
      mediator.publish('requestActivePerson');
    }

    $previewTab = $('.previewTab');
    $previewTab.html(templateList());
    $previewTab.removeClass('hidden');
    $preview = $previewTab.find('.preview');
    $hidePreviewButton = $previewTab.find('.hidePreviewButton');

    $hidePreviewButton.click(function () {
      return function () {
        mediator.publish('hidePreview');
      }
    }());

    i = 0;
    _.forEach(attributes, function (attrValue, attrKey){
      $preview.append(templateListElement({attribute: attributes[attrKey], value: group.getStudent(activePerson)[i]}));
      i++;
    });
  };

  this.updateActivePerson = function (value) {
    activePerson = value;
  };

  this.hidePreview = function () {
    $previewTab.addClass('hidden');
  };

  return this;
}