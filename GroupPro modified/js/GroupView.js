function GroupView () {
  this.init = function () {
    var templateGroupPro = _.template('<div class="selectorTab tab">' +
      '</div>' +
      '<div class="shortView view hidden">' +
      '</div>' +
      '<div class="previewTab tab hidden">' +
      '</div>'),
      $mainGroupProDiv = $('#personalInfo');

    $mainGroupProDiv.html(templateGroupPro);

    group.getData();
    this.refreshList();
  };

  this.refreshList = function () {
    var template = _.template('<ul class="personList"></ul><button class="serviceButtons createButton">Create New Person</button>');

    $(".selectorTab").html(template);

    for(var i = 0; i < group.getLength(); i++){
      mediator.publish('showPersonInGroupList', i);
    }

    $(".selectorTab .createButton").click(function() {
      return function() {
        mediator.publish('createPerson');
      }
    }());
  };

  return this;
}