function GroupMemberView () {
  var template  = _.template('<li class="personListElement"><%= name %> <%= surname %></li>');

  this.addPersonToList = function (personId) {
    $('.personList').append(template({name: group.getStudent(personId)[0], surname: group.getStudent(personId)[1][0]}));

    $('.personList .personListElement').last().click(function(personId) {
      return function() {
        mediator.publish('showPerson', personId);
      }
    }(personId));
  };

  this.refreshPersonInList = function(personId) {
    $('.personListElement').eq(personId).html(group.getStudent(personId)[0]+ ' ' + group.getStudent(personId)[1][0]);
  };

  return this;
}